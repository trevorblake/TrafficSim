# TrafficSim

![Splashscreen Logo](https://c0.wallpaperflare.com/preview/993/190/471/mauritius-road-peace-travel.jpg)

TrafficSim is a fully customizable virtual reality driving simulator developed in Unreal Engine 5.0.3/5.1.1 by Citrus Squeezers, a CSUS senior project team.

TrafficSim's purpose is to track data of the user for CSUS  Civil Engineering research purposes involving handling distractions, reaction times, and different driving scenarios.

This is the first project to be added to the new VR Laboratory in Del Norte Hall, and the project is lead by Dr. Ghazan Khan, the department chair of Civil Engineering at CSUS. 

The distributable runs in the lab using an Oculus Quest 2, as well as a Logitech G29 steering wheel and pedals, but can also be ran with any Windows 10/11 computer, without any of the other required hardware. Skip to [Deployment](#deployment) if you want to download and run the project.

This is an ongoing CSUS project, which could take years to reach its full potential.

# Contributors

Credits to the amazing team I was fortunate enough to have, who all helped immensely in development of this massive year-long project:

**Jacob Hallum, Kareem Moustafa, Amad Shah, Jacob Lay, Amanjot Singh, Hartej Singh, Nicole Monroy, and Ivan Gutierrez.**

If you need to reach out to our team regarding the project, please feel free to email me at: [trevorallenblake@gmail.com](mailto:trevorallenblake@gmail.com).

# Timeline

Our first semester, we created a minimum viable product, the basic tools and functionalities needed to begin designing our prebuilt maps.

Our second semester, we received constant feedback, spiked, and fine-tuned our features and have also developed 4 prebuilt scenarios.

## What We Have Developed:

- ### Adjustable Weather (Completely Customizable With Prebuilt Types Included)

**Rain:**
![Rain](/uploads/fee7fd2257ad376dbd5c8744838742d5/Rain.png)

**Snow:**
![snow](/uploads/54001a008ba0f772cea7533aaa5b8119/snow.png)

**Duststorm:**
![dust](/uploads/63a2bba36112364cb1faf4f10c7d4e02/dust.png)

**Fog:**
![fog](/uploads/a5df8368164a3958703dd4bf4889cc79/fog.png)

**Cloudy:**
![cloud](/uploads/4a1f2d441859e133df294da268384832/cloud.png)

**Presets:**
![prebuilts](/uploads/881aed9c60f6f83f3da39ff15a25217c/prebuilts.png)
There is also the ability to create entirely custom weather through the sliders within the settings tab.
<br/><br/>
- ### Day And Night 24hr Adjustable Cycle

**Morning:**
![morning](/uploads/d8ae99329e5f2140234da8b76288a9e9/morning.png)

**Afternoon:**
![midday](/uploads/d2d41b59bf59434872923f59cfd7c31c/midday.png)

**Night:**
![night](/uploads/a207f01c2ed92dc620bf7bd6900cb930/night.png)
The day and night cycle is setup to be animated to follow a real time scale but can also be toggled off, and is adjustable within the settings tab.
<br/><br/>
- ### AI Animals (Hazards) With Customizable Pathing

**Deer Hazard:**
![hazards](/uploads/3d85a0a37ca50ac13c081eaeb8e89886/hazards.png)
Also includes fox and birds, and all hazards can be toggled on or off and locations will be randomized based on spacing.
<br/><br/>
- ### AI Vehicles And Pedestrians With Randomized Road Pathing Including Traffic Control

**Vehicles:**
![Vehicles](/uploads/c08f87f45bbcb89f1f42cb1e49cf1a5f/Vehicles.png)

**Pedestrians:** 
![pedestrians](/uploads/62ae0fe6b99185f0f0227bee6772b1f8/pedestrians.png)
All vehicle and pedestrian movement within our city map is handled using traffic light and stop sign logic.
<br/><br/>
- ### User Interface And Menus

**Main Menu:**
![Main_Menu](/uploads/0ecc5504e3ec58d22ef75ca12502693e/Main_Menu.png)

**Map Selection Menu:**
![Select_Map_Menu](/uploads/b7418a2a3c8157a9310dfe45ca888e8b/Select_Map_Menu.png)

**Live Tab:**
![Live_Menu](/uploads/a935cb458f37a40e4e0ead714e737fe0/Live_Menu.png)

**Settings Tab:**
![Settings_Menu](/uploads/3d71e886c690879f16700660289abf8c/Settings_Menu.png)

**Logging Tab:**
![Logging_Menu](/uploads/533d1fb0c1bfd7c2171471fa09057322/Logging_Menu.png)

**Map Menu Tab:**
![Map_Menu](/uploads/7b152f5647547b1a02fd9322bc8990b1/Map_Menu.png)
<br/><br/>
- ### Maps

**City Map:**
![CityMap](/uploads/af7369c61d4d0550c18d9e1388297875/CityMap.jpg)

**Mountain Map:**
![MountainMap](/uploads/46222d99c5ec1ed2bfabc4e2bde546f0/MountainMap.jpg)

**Highway Map:**
![HighwayMap](/uploads/fc71a27b4551523d6f57c9044c244c24/HighwayMap.jpg)

**Freeway Map:**
![FreewayMap](/uploads/13d66b11a198961dbf0aaaec5369f5eb/FreewayMap.jpg)

# Developer Instructions

Unreal Engine 5.1.1 or newer and Microsoft Visual Studio 2019/2022 are required in order to **edit** the project in Unreal Engine.

However, if you simply want to **run** the project, all that is required is to have a Windows 10/11 PC and download our packaged project here: [Deployment](#deployment).

Unreal Engine 5.1.1 or newer can be installed through the [Epic Games Launcher](https://store.epicgames.com/en-US/download).

Visual Studio 2019/2022 installation can be found here: [Visual Studio Download](https://visualstudio.microsoft.com/vs/community/).

When downloading Visual Studio, it is important to select the correct options at this prompt menu and also install the newest Windows 10 SDK. (found on the right hand side of same prompt menu)

![Visual Studio Prompts](https://gitlab.com/trevorblake/TrafficSim/uploads/0bf61eafbe69b8b6f3f575a43fcab912/image.png)

Lastly, you can download the project using the following code in your command prompt:

```
git clone git@gitlab.com:trevorblake/TrafficSim.git
```

Further packages will be needed, in order to include all assets (with a file size of nearly 100GB), downloading the rest of the files is required for full editor functionality: [Project Files](https://drive.google.com/drive/folders/1k0AOF5my0YDM_6b9p3FLuTJvPV-oGEd0?usp=sharing).

# Testing

Functional, smoke, and acceptance testing were handled gracefully throughout the entirety of our project. Game development requires intensive testing of every single feature that has been implemented, and we have done extremely rigorous testing in regards to our Unreal Engine Editor builds prior to every single commit within our repository, as well as held special focus and time on our packaged and completed executable project. We have been entirely focused on the requirements and hopes of our client, Dr. Ghazan Khan, and his desire of a product meeting those expectations and more, and hope that we have delivered a product that has no errors, bugs, or breakable features that would hinder him from enjoying and using this project for his department's beneficial research. If you find any issues, please feel free to report them to our repository. 

# Deployment

Here is a compressed Google Drive download link for our project: [TrafficSim](https://drive.google.com/file/d/1-VLCJ1yM0mELlOEff8kDCfAOlPKVCiqd/view).

All that is required is to extract the folder and then open the TrafficSimUE5.exe located here: ```Windows\TrafficSimUE5\Binaries\Win64```

**NOTE:** This project has only been built, packaged, and tested on Windows 10/11, with DirectX 12 compatable graphics cards so be wary of any underlying issues.

# User Manual

For controls, and how to navigate through the simulator, read through our user manual [here](https://docs.google.com/document/d/1jF3Te_tbGibiQbgwcvqE2zefN3s6kiQS1Uy2uohrT8w/edit?usp=sharing).
